<%@ page import="java.io.PrintWriter" %>
<%@ page import="fr.JeeProjet.model.Article" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: adry6
  Date: 27/04/2018
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="css/style.css" rel="stylesheet">
<html>
<head>
    <title>Blog - RugbEasy</title>
</head>
<header>
    <jsp:include page="entete.jsp"/>
</header>
<body class="HomeWallpaper" >
</body>
<%
    HttpSession maSession = request.getSession();
    List<Article> listArticle;

    listArticle = (List) request.getAttribute("listArticle");

%>
<div class="contente">
    <c:forEach items="${ listArticle }" var="article">
        <div class ="Une" id="${ article.id }" \>
            <img height='365px' width='650'  src=img/blog/<c:out value="${ article.photo }" />  alt="Mountain View" />
            <div class="NomCate">
                <a class="lienCategorie" href="http://localhost:8082/${ article.nomChampionnat }">
                    <span style="margin-top: 10px">${ article.nomChampionnat }</span>
                </a>
            </div>
            <div class="blockTitreArticle">
                <a href="#" class="TitreArticle"> <c:out value="${ article.titre }"/></a>
                <br>
                <span  class="auteurArticle"  id="${ article.idUtilisateur }" >Le ${ article.dateCreation }</span>
            </div>

           <!-- id=\"" + article.idUtilisateur + "\"> Le " + article.dateCreation + "</span>");-->

        </div>
    </c:forEach>
</div>







<%
    /*
    PrintWriter pou = response.getWriter();
    HttpSession maSession = request.getSession();
    List<Article> listArticle;

    listArticle = (List) request.getAttribute("listArticle");

    if(listArticle != null) {
        for (Article article : listArticle) {

            pou.println("<div class =\"Une\" id=\"article" + article.id + "\">");
            pou.println("<img height='365px' width='650'  src='img/blog/" + article.photo + "' alt=\"Mountain View\"><div class=\"NomCate\"><span style=\"margin-top: 10px\"></span></div><br>");

            pou.println("<div class=\"blockTitreArticle\">");
            pou.println("<a href=\"#\" class=\"TitreArticle\">" + article.titre + "</a>");
            pou.println("<br>");
            pou.println("<span class=\"auteurArticle\" id=\"" + article.idUtilisateur + "\"> Le " + article.dateCreation + "</span>");
            pou.println("</div>");
            pou.println("</div>");
            continue;
        }
    }else {
        pou.println("<H2 style=\"font-size:30px;text-align: center\">Il n'y a aucun article à afficher pour le moment</H2>");
    }*/
%>
<footer>
    <jsp:include page="footer.jsp"/>
</footer>
</html>
