<%@ page import="fr.JeeProjet.model.Article" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.List" %>
<%@ page import="fr.JeeProjet.model.utilisateur" %>
<%--
  Created by IntelliJ IDEA.
  User: adry6
  Date: 04/04/2018
  Time: 10:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>

<body>
    <H1 style="font-size:34px;text-align: center"><u>A La Une</u> </H1>
        <%
            PrintWriter pou = response.getWriter();
            HttpSession maSession = request.getSession();
            List<Article> listArticle;

            listArticle = (List) request.getAttribute("listArticle");

            if(listArticle != null) {
              %>
                <c:forEach items="${ listArticle }" var="article">
                    <div class ="Une" id="${ article.id }" \>
                        <img height='365px' width='650'  src=img/blog/<c:out value="${ article.photo }" />  alt="Mountain View" />
                        <div class="NomCate">
                            <a class="lienCategorie" href="http://localhost:8082/${ article.nomChampionnat }">
                                <span style="margin-top: 10px">${ article.nomChampionnat }</span>
                            </a>
                        </div>
                        <div class="blockTitreArticle">
                            <a href="#" class="TitreArticle"> <c:out value="${ article.titre }"/></a>
                            <br>
                            <span  class="auteurArticle"  id="${ article.idUtilisateur }" >Le ${ article.dateCreation }</span>
                        </div>
                    </div>
                </c:forEach>
                <br>
                <br>
                <%
            }else {
                pou.println("<H2 style=\"font-size:30px;text-align: center\">Il n'y a aucun article à afficher pour le moment</H2>");
            }
        %>
    </body>
</html>
