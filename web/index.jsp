<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: adry6
  Date: 03/04/2018
  Time: 09:00
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>RugbEasy</title>
    <link href="css/style.css" rel="stylesheet">
  </head>
  <body class="HomeWallpaper" >
  <jsp:include page="entete.jsp"/>
    <div class="BandeauGauche">
      <jsp:include page="bandeau_gauche.jsp"/>
    </div>
    <div class="centrale">
      <jsp:include page="centrale.jsp"/>
    </div>
    <div class="BandeauDroite">
      <jsp:include page="bandeau_droite.jsp"/>
    </div>

    <div class ="footer">
      <jsp:include page="footer.jsp"/>
    </div>



  </body>
</html>
