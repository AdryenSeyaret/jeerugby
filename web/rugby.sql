-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 03 Juin 2018 à 16:27
-- Version du serveur :  5.6.20-log
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `rugby`
--

-- --------------------------------------------------------

--
-- Structure de la table `apourfavori`
--

CREATE TABLE IF NOT EXISTS `apourfavori` (
  `id` int(11) NOT NULL,
  `id_club` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
`id` int(11) NOT NULL,
  `titre` varchar(50) DEFAULT NULL,
  `contenu` varchar(1000) DEFAULT NULL,
  `dateCreation` varchar(10) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `article_tag`
--

CREATE TABLE IF NOT EXISTS `article_tag` (
  `id` int(11) NOT NULL,
  `id_article` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `championnat`
--

CREATE TABLE IF NOT EXISTS `championnat` (
`id` int(11) NOT NULL,
  `nomChampionnat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `championnat`
--

INSERT INTO `championnat` (`id`, `nomChampionnat`) VALUES
(1, 'Top14'),
(2, 'ProD2'),
(3, 'Top8');

-- --------------------------------------------------------

--
-- Structure de la table `classement`
--

CREATE TABLE IF NOT EXISTS `classement` (
`id` int(11) NOT NULL,
  `id_Championnat` int(11) NOT NULL DEFAULT '1',
  `id_Club` int(11) NOT NULL DEFAULT '1',
  `rang` int(11) DEFAULT NULL,
  `pts` int(11) DEFAULT NULL,
  `j` int(11) DEFAULT NULL,
  `g` int(11) DEFAULT NULL,
  `n` int(11) DEFAULT NULL,
  `p` int(11) DEFAULT NULL,
  `bp` int(11) DEFAULT NULL,
  `bc` int(11) DEFAULT NULL,
  `diffPoint` int(11) DEFAULT NULL,
  `cat` varchar(25) DEFAULT NULL,
  `nat` varchar(25) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `classement`
--

INSERT INTO `classement` (`id`, `id_Championnat`, `id_Club`, `rang`, `pts`, `j`, `g`, `n`, `p`, `bp`, `bc`, `diffPoint`, `cat`, `nat`) VALUES
(1, 1, 1, 5, 70, 26, 15, 0, 11, 689, 541, 148, NULL, NULL),
(2, 1, 2, 3, 74, 26, 16, 1, 9, 719, 595, 124, NULL, NULL),
(3, 2, 15, 1, 97, 30, 20, 1, 9, 919, 571, 348, NULL, NULL),
(4, 1, 3, 11, 46, 26, 10, 0, 16, 537, 757, -220, NULL, NULL),
(7, 1, 4, 10, 48, 26, 10, 1, 15, 557, 623, -66, NULL, NULL),
(8, 1, 5, 14, 36, 26, 7, 1, 18, 485, 726, -241, NULL, NULL),
(9, 1, 6, 6, 69, 26, 15, 0, 11, 638, 624, 14, NULL, NULL),
(10, 1, 7, 9, 54, 26, 11, 1, 14, 629, 687, -58, NULL, NULL),
(11, 1, 8, 7, 67, 26, 14, 1, 11, 686, 531, 155, NULL, NULL),
(12, 1, 9, 1, 81, 26, 17, 0, 9, 752, 559, 193, NULL, NULL),
(13, 1, 10, 13, 39, 26, 7, 3, 16, 566, 824, -258, NULL, NULL),
(14, 1, 11, 12, 42, 26, 9, 0, 17, 540, 740, -200, NULL, NULL),
(15, 1, 12, 8, 66, 26, 15, 0, 11, 590, 584, 6, NULL, NULL),
(16, 1, 13, 2, 80, 26, 18, 0, 8, 622, 478, 144, NULL, NULL),
(17, 1, 14, 4, 73, 26, 14, 0, 12, 766, 507, 259, NULL, NULL),
(18, 2, 16, 2, 92, 30, 20, 2, 8, 679, 546, 133, NULL, NULL),
(19, 2, 19, 3, 89, 30, 20, 1, 9, 775, 667, 108, NULL, NULL),
(20, 2, 20, 4, 84, 30, 17, 0, 13, 753, 540, 213, NULL, NULL),
(21, 2, 21, 5, 84, 30, 19, 1, 10, 756, 670, 86, NULL, NULL),
(22, 2, 22, 6, 80, 30, 17, 0, 13, 789, 694, 95, NULL, NULL),
(23, 2, 23, 7, 69, 30, 15, 0, 15, 656, 580, 76, NULL, NULL),
(24, 2, 24, 7, 69, 30, 15, 0, 15, 656, 580, 76, NULL, NULL),
(25, 2, 24, 8, 69, 30, 14, 1, 15, 732, 764, -32, NULL, NULL),
(26, 2, 25, 9, 69, 30, 14, 1, 15, 637, 678, -41, NULL, NULL),
(27, 2, 26, 10, 62, 30, 12, 0, 18, 721, 739, -18, NULL, NULL),
(28, 2, 27, 11, 61, 30, 13, 1, 16, 641, 716, -75, NULL, NULL),
(29, 2, 28, 12, 59, 30, 13, 0, 17, 633, 682, -49, NULL, NULL),
(30, 2, 29, 13, 59, 30, 13, 1, 16, 592, 661, -69, NULL, NULL),
(31, 2, 30, 14, 53, 30, 11, 0, 19, 585, 767, -182, NULL, NULL),
(32, 2, 31, 15, 46, 30, 9, 1, 20, 602, 767, -165, NULL, NULL),
(33, 2, 32, 16, 35, 30, 7, 2, 21, 547, 975, -428, NULL, NULL),
(34, 3, 33, 1, 54, 14, 11, 1, 2, 408, 188, 220, NULL, NULL),
(35, 3, 34, 2, 51, 14, 11, 1, 2, 317, 189, 128, NULL, NULL),
(36, 3, 35, 3, 43, 14, 8, 2, 4, 247, 167, 80, NULL, NULL),
(37, 3, 36, 4, 41, 14, 9, 0, 5, 296, 253, 43, NULL, NULL),
(38, 3, 37, 5, 30, 14, 6, 0, 8, 288, 272, 16, NULL, NULL),
(39, 3, 38, 6, 22, 14, 4, 0, 10, 209, 341, -132, NULL, NULL),
(40, 3, 39, 7, 14, 14, 2, 1, 11, 186, 323, -137, NULL, NULL),
(41, 3, 40, 8, 11, 14, 2, 1, 11, 163, 381, -218, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `club`
--

CREATE TABLE IF NOT EXISTS `club` (
`id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `nomcourt` varchar(15) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  `dateFondation` varchar(10) DEFAULT NULL,
  `president` varchar(50) DEFAULT NULL,
  `entraineur` varchar(50) DEFAULT NULL,
  `nomStade` varchar(50) DEFAULT NULL,
  `cap` int(11) DEFAULT NULL,
  `stadeAdresse` varchar(100) DEFAULT NULL,
  `stadeAdresseSuite` varchar(100) DEFAULT NULL,
  `stadeTelephone` int(11) DEFAULT NULL,
  `id_Championnat` int(11) NOT NULL DEFAULT '1',
  `id_paysClub` int(11) NOT NULL DEFAULT '1',
  `photoClub` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Contenu de la table `club`
--

INSERT INTO `club` (`id`, `nom`, `nomcourt`, `pays`, `dateFondation`, `president`, `entraineur`, `nomStade`, `cap`, `stadeAdresse`, `stadeAdresseSuite`, `stadeTelephone`, `id_Championnat`, `id_paysClub`, `photoClub`) VALUES
(1, 'Lyon Lou', 'Lou', 'France', '1896', 'Yann Roubert', 'Sébastien Bruno', 'Stade de Gerland', 40494, '353 Avenue Jean Jaurès, 69007 Lyon', NULL, 1111111, 1, 1, 'lou.png'),
(2, 'Stade Toulousais', 'Toulouse', 'France', '1907', 'Didier Lacroix', 'Ugo Mola', 'Ernest Wallon', 31200, '114 rue des Troènes, 31200 Toulouse', NULL, 892693115, 1, 1, 'Stade_toulousain.png'),
(3, 'Agen', 'Agen', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Agen.png'),
(4, 'Bordeaux Bègle', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Bordeau.png'),
(5, 'Brive', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Brive.png'),
(6, 'Castres', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Castres.png'),
(7, 'Clermont', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Clermont.png'),
(8, 'La Rochelle', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'LaRochelle.png'),
(9, 'Montpellier', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Montpeller.png'),
(10, 'Oyonnax', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Oyonnax.png'),
(11, 'Paris', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Paris.png'),
(12, 'Pau', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Pau.png'),
(13, 'Racing 92', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Racing_92.png'),
(14, 'Toulon', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Toulon.png'),
(15, 'Perpignan', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Perpigan.png'),
(16, 'Montauban', NULL, 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Mauntauban.png'),
(19, 'Grenoble', 'Grenoble', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Grenoble.png'),
(20, 'Mont-de-Marsan', 'Mont-de-Marsan', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Mont-de-Marsan.png'),
(21, 'Béziers', 'Béziers', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Beziers.png'),
(22, 'Biarritz', 'Biarritz', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Biarritz.png'),
(23, 'Nevers', 'Nevers', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Nevers.png'),
(24, 'Bayonne', 'Bayonne', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Bayonne.png'),
(25, 'Colomiers', 'Colomiers', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Colomiers.png'),
(26, 'Vannes', 'Vannes', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Vannes.png'),
(27, 'Aurillac', 'Aurillac', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Aurillac.png'),
(28, 'Massy', 'Massy', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Massy.png'),
(29, 'Soyaux Angoulême', 'Soyaux ', 'France', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, 2, 1, 'Soyaux.png'),
(30, 'Carcassonne', 'Carcassonne', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Carcassonne.png'),
(31, 'Dax', 'Dax', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Dax.png'),
(32, 'Narbonne', 'Narbonne', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1, 'Narbonne.png'),
(33, 'Montpellier', 'Montpellier', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Montpeller.png'),
(34, 'Villeneuve d''Ascq', 'Villeneuve', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Villeneuve.png'),
(35, 'Toulouse', 'Toulouse', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Stade_toulousain.png'),
(36, 'Blagnac', 'Blagnac', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Blagnac.png'),
(37, 'Stade Rennais', 'Rennes', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Rennes.png'),
(38, 'AS Bayonne', 'Bayonne', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Bayonne.png'),
(39, 'Bobigny', 'Bobigny', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Bobigny.png'),
(40, 'Romagnat', 'Romagnat', 'France', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1, 'Clermont.png');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
`id` int(11) NOT NULL,
  `contenu` varchar(250) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `id_article` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `estcompetiteur`
--

CREATE TABLE IF NOT EXISTS `estcompetiteur` (
  `id` int(11) NOT NULL,
  `id_Championnat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `journee`
--

CREATE TABLE IF NOT EXISTS `journee` (
`id` int(11) NOT NULL,
  `nbj` int(11) DEFAULT NULL,
  `id_Championnat` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `journee`
--

INSERT INTO `journee` (`id`, `nbj`, `id_Championnat`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(6, 2, 3),
(7, 3, 1),
(8, 3, 2),
(9, 3, 3),
(10, 4, 1),
(11, 4, 2),
(12, 4, 3),
(13, 5, 1),
(14, 5, 2),
(15, 5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `matche`
--

CREATE TABLE IF NOT EXISTS `matche` (
`id` int(11) NOT NULL,
  `id_journee` int(11) NOT NULL DEFAULT '1',
  `id_club_domicile` int(11) DEFAULT NULL,
  `id_club_visiteur` int(11) DEFAULT NULL,
  `id_championnat` int(11) DEFAULT NULL,
  `dateMatche` varchar(10) DEFAULT NULL,
  `heurematche` varchar(50) DEFAULT NULL,
  `scoredom` int(11) DEFAULT NULL,
  `scorevisi` int(11) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `matche`
--

INSERT INTO `matche` (`id`, `id_journee`, `id_club_domicile`, `id_club_visiteur`, `id_championnat`, `dateMatche`, `heurematche`, `scoredom`, `scorevisi`) VALUES
(1, 1, 1, 2, 1, '12/08/2017', '18:00:00', 68, 25),
(2, 5, 15, 16, 2, '12/09/2017', '20:00:00', 52, 45);

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

CREATE TABLE IF NOT EXISTS `participe` (
  `id` int(11) NOT NULL,
  `id_club` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `paysclub`
--

CREATE TABLE IF NOT EXISTS `paysclub` (
`id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `paysclub`
--

INSERT INTO `paysclub` (`id`, `nom`) VALUES
(1, 'France');

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
`id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
`id` int(11) NOT NULL,
  `nomUti` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `mdp` varchar(25) DEFAULT NULL,
  `confmdp` varchar(25) DEFAULT NULL,
  `civilite` int(11) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `adresseSuite` varchar(100) DEFAULT NULL,
  `codePostal` int(11) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `nbabFF` int(11) DEFAULT NULL,
  `ddn` varchar(10) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  `catPro` varchar(50) DEFAULT NULL,
  `spoPrat` varchar(50) DEFAULT NULL,
  `equiFav1` varchar(50) DEFAULT NULL,
  `equiFav2` varchar(50) DEFAULT NULL,
  `equiFav3` varchar(50) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `offresFF` tinyint(1) DEFAULT NULL,
  `isEditeur` tinyint(1) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nomUti`, `email`, `mdp`, `confmdp`, `civilite`, `nom`, `prenom`, `adresse`, `adresseSuite`, `codePostal`, `ville`, `telephone`, `nbabFF`, `ddn`, `pays`, `etat`, `catPro`, `spoPrat`, `equiFav1`, `equiFav2`, `equiFav3`, `newsletter`, `offresFF`, `isEditeur`, `isAdmin`) VALUES
(1, 'Admin', 'admin@mail.fr', '123', '123', 1, 'admin', 'admin', '1 rue de la République', NULL, 69001, 'Lyon', 123456789, 12345, 'what', 'france', 'Rhone alpes', 'Pro', 'Rugby', 'Lou', 'Toulon', 'Montpellier', 1, 1, 1, 1),
(2, 'Juste', 'juste69@gmail.com', '123', '123', 1, 'Leblanc', 'Juste', '1 rue du faux', NULL, 69009, 'lyon', 401548796, 45875, '27-06-1985', 'france', 'rhone', 'amateur', 'foot', 'lyon', NULL, NULL, 1, 1, 0, 0),
(3, 'Marlène', 'soeurette85@gmail.com', '123', '123', 0, 'Sasoeur', 'Marlène', '2 rue du vieux renversé', NULL, 85001, 'La Roche-sur-Yon', 612458795, 146448, '12-02-1992', 'france', NULL, 'd2', 'rugby', 'Loup', NULL, NULL, 0, 1, 1, 0),
(4, 'Pignon', 'F.Pignon@gmail.com', '123', '123', 1, 'Pignon', 'François', '3 boulevard du diner', NULL, 42001, 'Saint etienne', 624586317, 146946, '21-03-1981', 'Belgique', NULL, NULL, 'aucun', NULL, NULL, NULL, 1, 1, 1, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `apourfavori`
--
ALTER TABLE `apourfavori`
 ADD PRIMARY KEY (`id`,`id_club`), ADD KEY `FK_aPourFavori_id_club` (`id_club`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_article_id_utilisateur` (`id_utilisateur`);

--
-- Index pour la table `article_tag`
--
ALTER TABLE `article_tag`
 ADD PRIMARY KEY (`id`,`id_article`), ADD KEY `FK_article_tag_id_article` (`id_article`);

--
-- Index pour la table `championnat`
--
ALTER TABLE `championnat`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `classement`
--
ALTER TABLE `classement`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_classement_id_Championnat` (`id_Championnat`), ADD KEY `FK_classement_id_Club` (`id_Club`);

--
-- Index pour la table `club`
--
ALTER TABLE `club`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_club_id_paysClub` (`id_paysClub`), ADD KEY `FK_club_id_Championnat` (`id_Championnat`);

--
-- Index pour la table `commentaire`
--
ALTER TABLE `commentaire`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_commentaire_id_utilisateur` (`id_utilisateur`), ADD KEY `FK_commentaire_id_article` (`id_article`);

--
-- Index pour la table `estcompetiteur`
--
ALTER TABLE `estcompetiteur`
 ADD PRIMARY KEY (`id`,`id_Championnat`), ADD KEY `FK_estCompetiteur_id_Championnat` (`id_Championnat`);

--
-- Index pour la table `journee`
--
ALTER TABLE `journee`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_journee_id_Championnat` (`id_Championnat`);

--
-- Index pour la table `matche`
--
ALTER TABLE `matche`
 ADD PRIMARY KEY (`id`), ADD KEY `FK_matche_id_journee` (`id_journee`), ADD KEY `FK_matche_id_club_domicile` (`id_club_domicile`), ADD KEY `FK_matche_id_club_visiteur` (`id_club_visiteur`), ADD KEY `FK_matche_id_club_championnat` (`id_championnat`);

--
-- Index pour la table `participe`
--
ALTER TABLE `participe`
 ADD PRIMARY KEY (`id`,`id_club`), ADD KEY `FK_participe_id_club` (`id_club`);

--
-- Index pour la table `paysclub`
--
ALTER TABLE `paysclub`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tag`
--
ALTER TABLE `tag`
 ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `article`
--
ALTER TABLE `article`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `championnat`
--
ALTER TABLE `championnat`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `classement`
--
ALTER TABLE `classement`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT pour la table `club`
--
ALTER TABLE `club`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `commentaire`
--
ALTER TABLE `commentaire`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `journee`
--
ALTER TABLE `journee`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `matche`
--
ALTER TABLE `matche`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `paysclub`
--
ALTER TABLE `paysclub`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `tag`
--
ALTER TABLE `tag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `apourfavori`
--
ALTER TABLE `apourfavori`
ADD CONSTRAINT `FK_aPourFavori_id` FOREIGN KEY (`id`) REFERENCES `utilisateur` (`id`),
ADD CONSTRAINT `FK_aPourFavori_id_club` FOREIGN KEY (`id_club`) REFERENCES `club` (`id`);

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
ADD CONSTRAINT `FK_article_id_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `article_tag`
--
ALTER TABLE `article_tag`
ADD CONSTRAINT `FK_article_tag_id` FOREIGN KEY (`id`) REFERENCES `tag` (`id`),
ADD CONSTRAINT `FK_article_tag_id_article` FOREIGN KEY (`id_article`) REFERENCES `article` (`id`);

--
-- Contraintes pour la table `classement`
--
ALTER TABLE `classement`
ADD CONSTRAINT `FK_classement_id_Championnat` FOREIGN KEY (`id_Championnat`) REFERENCES `championnat` (`id`),
ADD CONSTRAINT `FK_classement_id_Club` FOREIGN KEY (`id_Club`) REFERENCES `club` (`id`);

--
-- Contraintes pour la table `club`
--
ALTER TABLE `club`
ADD CONSTRAINT `FK_club_id_Championnat` FOREIGN KEY (`id_Championnat`) REFERENCES `championnat` (`id`),
ADD CONSTRAINT `FK_club_id_paysClub` FOREIGN KEY (`id_paysClub`) REFERENCES `paysclub` (`id`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
ADD CONSTRAINT `FK_commentaire_id_article` FOREIGN KEY (`id_article`) REFERENCES `article` (`id`),
ADD CONSTRAINT `FK_commentaire_id_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `estcompetiteur`
--
ALTER TABLE `estcompetiteur`
ADD CONSTRAINT `FK_estCompetiteur_id` FOREIGN KEY (`id`) REFERENCES `club` (`id`),
ADD CONSTRAINT `FK_estCompetiteur_id_Championnat` FOREIGN KEY (`id_Championnat`) REFERENCES `championnat` (`id`);

--
-- Contraintes pour la table `journee`
--
ALTER TABLE `journee`
ADD CONSTRAINT `FK_journee_id_Championnat` FOREIGN KEY (`id_Championnat`) REFERENCES `championnat` (`id`);

--
-- Contraintes pour la table `matche`
--
ALTER TABLE `matche`
ADD CONSTRAINT `FK_club_visiteur` FOREIGN KEY (`id_club_visiteur`) REFERENCES `club` (`id`),
ADD CONSTRAINT `FK_id_club_championnat` FOREIGN KEY (`id_championnat`) REFERENCES `championnat` (`id`),
ADD CONSTRAINT `FK_matche_id_domicile` FOREIGN KEY (`id_club_domicile`) REFERENCES `club` (`id`),
ADD CONSTRAINT `FK_matche_id_journee` FOREIGN KEY (`id_journee`) REFERENCES `journee` (`id`);

--
-- Contraintes pour la table `participe`
--
ALTER TABLE `participe`
ADD CONSTRAINT `FK_participe_id` FOREIGN KEY (`id`) REFERENCES `matche` (`id`),
ADD CONSTRAINT `FK_participe_id_club` FOREIGN KEY (`id_club`) REFERENCES `club` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
