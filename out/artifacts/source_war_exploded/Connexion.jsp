<%@ page import="fr.JeeProjet.model.utilisateur" %><%--
  Created by IntelliJ IDEA.
  User: adry6
  Date: 13/05/2018
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="js/bootstrap.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

    <title>Connexion</title>
</head>
<body class="HomeWallpaper" >
<jsp:include page="entete.jsp"/>
<div class="centreConnect">
<br>


    <%
        utilisateur unUtilisateur;
        HttpSession maSession = request.getSession();
        unUtilisateur = (utilisateur) maSession.getAttribute("utilisateur");
        if(unUtilisateur != null) {
            %>
            <jsp:forward page="/MonCompte"/>
            <%
        }
    %>







    <h1 style="text-align: center;color:#00000e;">Se Connecter</h1>
    <br>
    <form name="myForm" action="/Connexion" method="post" style="text-align: center">
        <fieldset class="fieldsetConnect">

            Identifiant:<br>
            <i class="fa fa-envelope"></i>
            <input type="text" name="pseudo" placeholder="ex:toto" >
            <br>
            Mot de Passe:<br>
            <input type="password" name="mdp" >
            <br><br>
            <input type="submit" class="btn btn-primary" value="Connexion">
        </fieldset>
        <br><br>



    </form>

    <%

        int erreur = (int)request.getAttribute("error");
        int inscrit = (int)request.getAttribute("inscrit");
        if(inscrit ==1){
            out.println("<div style='color:green;'><center><u>Succès</u> <br>Votre Compte à bien été créé<br> Veuillez vous connecter</center></div>");
        }else{
            if(erreur ==1){
                out.println("<div style='color:red;'><center><u>Erreur</u> <br>Cette combinaison Email - Mot de passe n'existe pas !</center></div>");
            }
        }

    %>
    <br><br>
    <br>

    <br>
    <span style="text-align: center"> Pas encore de Compte?<a href="http://localhost:8082/Inscription"> Inscription</a></span>
</div>
</body>
</html>
