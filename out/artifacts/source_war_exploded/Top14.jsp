<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="fr.JeeProjet.model.Equipe" %>
<%@ page import="fr.JeeProjet.model.Classement" %>
<%@ page import="fr.JeeProjet.model.Article" %>
<%--
  Created by IntelliJ IDEA.
  User: adry6
  Date: 23/04/2018
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Top 14</title>


    <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="js/bootstrap.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>

</head>
<body>
<jsp:include page="entete.jsp"/>


<div class ="content">
    <div class="ConteneurImage">
        <img class = "ImgBandeau"  src="css/BandeauTop14.jpg" alt="Mountain View">
        <img class = "ImgTampon" style=" z-index: 0;" src="css/Top14Tampon.png" alt="Mountain View">
    </div>

    <div class="navi">

        <div class="nav">
            <ul>
                <li class="classement" ><a href="javascript:onClickClassement();" >Classement </a></li>
                <li class="resultat"><a href="javascript:onClickResult();">Resultats</a></li>
                <li class="equipe"><a href="javascript:onClickEquipe();">Equipes</a></li>
                <li class="infos"><a href="javascript:onClickBlog();">Infos</a></li>
            </ul>
        </div>
        <br>
        <br>
        <%
            HttpSession maSession = request.getSession();
            List<Classement> ClassementTop14 =new ArrayList();
            ClassementTop14 = (List) request.getAttribute("ClassementTop14");


        %>

        <div id = "BlockClassement">
            <table class="table table-dark ">
                <thead>
                <tr>
                    <th scope="col">Rang</th>
                    <th scope="col">Equipe</th>
                    <th scope="col">J</th>
                    <th scope="col">G</th>
                    <th scope="col">N</th>
                    <th scope="col">P</th>
                    <th scope="col">Bp</th>
                    <th scope="col">Bc</th>
                    <th scope="col">+/-</th>
                    <th scope="col">Pts</th>
                </tr>
                </thead>
                <tbody>

                <%
                    for(int y = 0;y<ClassementTop14.size();y++){
                        if(y == 12){
                            %>
                            <tr class="bg-warning">
                            <%

                        }else{
                           if(y == 13){
                                %>
                                    <tr class="bg-danger">
                                <%
                           }else{
                               %>
                                    <tr>
                                <%
                           }
                        }
                        %>
                            <th scope="row"><% out.println(y+1); %></th>
                            <td><%out.println(ClassementTop14.get(y).getNomClub());%></td>
                            <td><%out.println(ClassementTop14.get(y).getJ());%></td>
                            <td><%out.println(ClassementTop14.get(y).getG());%></td>
                            <td><%out.println(ClassementTop14.get(y).getN());%></td>
                            <td><%out.println(ClassementTop14.get(y).getP());%></td>
                            <td><%out.println(ClassementTop14.get(y).getBp());%></td>
                            <td><%out.println(ClassementTop14.get(y).getBc());%></td>
                            <td><%out.println(ClassementTop14.get(y).getDifPoint());%></td>
                            <td><%out.println(ClassementTop14.get(y).getPoints());%></td>
                        </tr>
                        <%

                    }

                %>


                </tbody>
            </table>
        </div>
        <div id = "BlockResult"> For each list match</div>
        <div id = "BlockEquipe" style="padding-left: 7%;">
            <div>
                    <%
                       List<Equipe> ListeEquipe = new ArrayList();

                        ListeEquipe = (List) request.getAttribute("ListeEquipe");
                        for(int i =0;i<ListeEquipe.size();i++){
                            if(i == 0){
                               %><div class="row"style="display:  flex;"><%
                            }
                            Equipe MesEquipes= null;
                            MesEquipes =ListeEquipe.get(i);
                            %>
                                <div class="card" style="border-radius:10px;background-color: rgba(0,0,0,.125);margin-left:10px;margin-right:25px;width: 18rem;text-align:center;border: 1px solid rgba(0,0,0,.125);">
                                    <img class="card-img-top" style="Margin-top:10px;" height = 100px src="img/Equipe/<% out.println(MesEquipes.getPhotoClub()); %>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="card-title"><% out.println(MesEquipes.getNom());%></h5>
                                        <a href="/Club?id=<% out.println(MesEquipes.getId()); %>" class="btn btn-primary">Plus de détail</a>
                                    </div>
                                </div>
                            <%
                                    if ((i % 4) == 3)
                                    {
                                            %>
                                             </div>
                                             <br>
                                             <div class="row"style="display:  flex;">
                                              <%
                                    }
                        }

                    %>





                </div>
                <br>
                <br>

            </div>

            <br>
            <br>
            <br>

        </div>
        <div id = "BlockBlog">
            <%
            List<Article> ListeArticle = new ArrayList();

            ListeArticle = (List) request.getAttribute("ListeArticle");
            %>

            <c:forEach items="${ ListeArticle }" var="article">
                <div class ="Une" id="${ article.id }" \>
                    <img height='365px' width='650'  src=img/blog/<c:out value="${ article.photo }" />  alt="Mountain View" />
                    <div class="NomCate">
                        <a class="lienCategorie" href="http://localhost:8082/${ article.nomChampionnat }">
                            <span style="margin-top: 10px">${ article.nomChampionnat }</span>
                        </a>
                    </div>
                    <div class="blockTitreArticle">
                        <a href="#" class="TitreArticle"> <c:out value="${ article.titre }"/></a>
                        <br>
                        <span  class="auteurArticle"  id="${ article.idUtilisateur }" >Le ${ article.dateCreation }</span>
                    </div>
                </div>
            </c:forEach>
            <br>
            <br>

        </div>

    </div>

</div>
</body>

<script>

    $( "#BlockClassement" ).hide();
    $( "#BlockResult" ).hide();
    $( "#BlockEquipe" ).hide();
    $( "#BlockBlog" ).hide();
    function onClickClassement(){
        $( "#BlockResult" ).hide();
        $( "#BlockEquipe" ).hide();
        $( "#BlockBlog" ).hide();
        $( "#BlockClassement" ).show();
    }
    function onClickResult(){
        $( "#BlockClassement" ).hide();
        $( "#BlockEquipe" ).hide();
        $( "#BlockBlog" ).hide();
        $( "#BlockResult" ).show();
    }
    function onClickEquipe(){
        $( "#BlockClassement" ).hide();
        $( "#BlockResult" ).hide();
        $( "#BlockBlog" ).hide();
        $( "#BlockEquipe" ).show();
    }
    function onClickBlog(){
        $( "#BlockClassement" ).hide();
        $( "#BlockResult" ).hide();
        $( "#BlockEquipe" ).hide();
        $( "#BlockBlog" ).show();
    }
</script>


</html>
