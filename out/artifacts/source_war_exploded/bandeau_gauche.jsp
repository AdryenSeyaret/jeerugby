<%--
  Created by IntelliJ IDEA.
  User: adry6
  Date: 04/04/2018
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="css/style.css" rel="stylesheet">
    <title>Title</title>
</head>
<body>
<u>
    <h2 style="text-align: center">Derniers Resultats</h2>
</u>

<!--
<br>

Top 14

<br>

CALENDRIER/RÉSULTATS 2017-2018

<br>

24e Journée :14/04/2018
<br>
<br>

Rugby Club Toulonnais 32 - 17 Montpellier Hérault Rugby
<br>

ASM Clermont 33 - 3 Union Bordeaux-Bègles
<br>

US Oyonnax 40 - 17 CA Brive-Corrèze
<br>

Section Paloise 22 - 33 SU Agen
<br>

Lyon LOU 44 - 3 Stade Français
<br>

15/04/2018

<br>

Stade Rochelais 18 - 26 Castres Olympique
<br>

Stade Toulousain 42 - 27 Racing 92
<br>

-->


<div class="resultBlock">
    <h3 style="text-align: center"> Top 14</h3>
    <div class="CONT"><div class="p-5"><div class="ligne off" ><div class="intitule"><span>
        <sup></sup>
    </span></div></div>
        <h3 class="color date-event">samedi 14 avril 2018 </h3>
        <div class="ligne bb-color " idClub1="23" idClub2="104"  idmatch="25837" >
            <div class="heure"><strong>14h45</strong>                          </div>
            <div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub23.html"><img src="//medias.lequipe.fr/logo-rugby/23/20"></a>
                <a href="/Rugby/RugbyFicheClub23.html" class="gagne">Toulon</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25837.html" >32-17</a></div>
            <div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub104.html"><img src="//medias.lequipe.fr/logo-rugby/104/20"></a><a href="/Rugby/RugbyFicheClub104.html" class="">Montpellier</a></div><div class="pariez"></div></div><div class="clear"></div><div class="ligne bb-color " idClub1="18" idClub2="1"  idmatch="25836" ><div class="heure"><strong>18h00</strong>                          </div><div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub18.html"><img src="//medias.lequipe.fr/logo-rugby/18/20"></a><a href="/Rugby/RugbyFicheClub18.html" class="">Pau</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25836.html" >22-33</a></div><div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub1.html"><img src="//medias.lequipe.fr/logo-rugby/1/20"></a><a href="/Rugby/RugbyFicheClub1.html" class="gagne">Agen</a></div><div class="pariez"></div></div><div class="clear"></div><div class="ligne bb-color " idClub1="500000000000162" idClub2="8"  idmatch="25835" ><div class="heure"><strong>18h00</strong>                          </div><div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub500000000000162.html"><img src="//medias.lequipe.fr/logo-rugby/500000000000162/20"></a><a href="/Rugby/RugbyFicheClub500000000000162.html" class="gagne">Oyonnax</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25835.html" >40-17</a></div><div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub8.html"><img src="//medias.lequipe.fr/logo-rugby/8/20"></a><a href="/Rugby/RugbyFicheClub8.html" class="">Brive</a></div><div class="pariez"></div></div><div class="clear"></div><div class="ligne bb-color " idClub1="14" idClub2="4000000000000170"  idmatch="25832" ><div class="heure"><strong>18h00</strong>                          </div><div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub14.html"><img src="//medias.lequipe.fr/logo-rugby/14/20"></a><a href="/Rugby/RugbyFicheClub14.html" class="gagne">Clermont</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25832.html" >33-3</a></div><div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub4000000000000170.html"><img src="//medias.lequipe.fr/logo-rugby/4000000000000170/20"></a><a href="/Rugby/RugbyFicheClub4000000000000170.html" class="">Bordeaux-B&egrave;gles</a></div><div class="pariez"></div></div><div class="clear"></div><div class="ligne bb-color " idClub1="114" idClub2="22"  idmatch="25834" ><div class="heure"><strong>20h45</strong>                          </div><div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub114.html"><img src="//medias.lequipe.fr/logo-rugby/114/20"></a><a href="/Rugby/RugbyFicheClub114.html" class="gagne">Lyon</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25834.html" >44-3</a></div><div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub22.html"><img src="//medias.lequipe.fr/logo-rugby/22/20"></a><a href="/Rugby/RugbyFicheClub22.html" class="">Stade Fran&ccedil;ais</a></div><div class="pariez"></div></div><div class="clear"></div> <h3 class="color date-event">dimanche 15 avril 2018 </h3>   <div class="ligne bb-color " idClub1="13" idClub2="9"  idmatch="25833" ><div class="heure"><strong>12h30</strong>                          </div><div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub13.html"><img src="//medias.lequipe.fr/logo-rugby/13/20"></a><a href="/Rugby/RugbyFicheClub13.html" class="">La Rochelle</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25833.html" >18-26</a></div><div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub9.html"><img src="//medias.lequipe.fr/logo-rugby/9/20"></a><a href="/Rugby/RugbyFicheClub9.html" class="gagne">Castres</a></div><div class="pariez"></div></div><div class="clear"></div><div class="ligne bb-color last-ligne" idClub1="24" idClub2="21"  idmatch="25838" ><div class="heure"><strong>16h50</strong>                          </div><div class="equipeDom"><a class="link" href="/Rugby/RugbyFicheClub24.html"><img src="//medias.lequipe.fr/logo-rugby/24/20"></a><a href="/Rugby/RugbyFicheClub24.html" class="gagne">Toulouse</a></div><div class="score"><a href="/Rugby/RugbyFicheMatch25838.html" >42-27</a></div><div class="equipeExt"><a class="link" href="/Rugby/RugbyFicheClub21.html"><img src="//medias.lequipe.fr/logo-rugby/21/20"></a><a href="/Rugby/RugbyFicheClub21.html" class="">Racing 92</a></div><div class="pariez"></div></div><div class="clear"></div><div class="clear"></div></div></div><div class="clear"></div></div></div></section></div><div class="col-droite">
    </div>
</div>


</div>



</body>
</html>
