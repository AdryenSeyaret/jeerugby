package fr.JeeProjet.model;

        import java.sql.Connection;
        import java.sql.DriverManager;
        import java.sql.SQLException;

public class model {

    public static Connection getConnection() throws SQLException, ClassNotFoundException {

        String url = "jdbc:mysql://localhost:3306/rugby?useSSL=false";
        //String url = "http://localhost:81/mysql";
        String user = "root";
        String password = "root";

        Class.forName("com.mysql.jdbc.Driver");

        Connection connection = DriverManager.getConnection(url, user, password);

        return connection;
    }

    public static void closeConnection(Connection connection) throws SQLException {
        connection.close();
    }
}