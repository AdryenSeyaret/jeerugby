package fr.JeeProjet.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Classement {
    public Classement(){}
    public int id;
    public String rang;
    public String nomClub;
    public String points;
    public String j;
       public String g;
    public String n;
    public String p;
    public String bp;
    public String bc;
    public String difPoint;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRang() {
        return rang;
    }

    public void setRang(String rang) {
        this.rang = rang;
    }

    public String getNomClub() {
        return nomClub;
    }

    public void setNomClub(String nomClub) {
        this.nomClub = nomClub;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getJ() {
        return j;
    }

    public void setJ(String j) {
        this.j = j;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getBp() {
        return bp;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }

    public String getBc() {
        return bc;
    }

    public void setBc(String bc) {
        this.bc = bc;
    }

    public String getDifPoint() {
        return difPoint;
    }

    public void setDifPoint(String difPoint) {
        this.difPoint = difPoint;
    }


    //
    //
    //


    private Connection connection;

    public Classement(Connection connection) {
        this.connection = connection;
    }

    public List<Classement> GetClassementTop14() throws SQLException {
        Statement statement = connection.createStatement();
        List<Classement> listTop14 = new ArrayList<>();
        String query = "SELECT classement.*, club.nom FROM classement,club WHERE classement.id_Club=club.id AND classement.id_Championnat = 1 ORDER BY classement.rang;";
        ResultSet rs = statement.executeQuery(query);
        while(rs.next()) {
            Classement top14 = new Classement();
            top14.setRang(rs.getString("rang"));
            top14.setNomClub(rs.getString("nom"));
            top14.setPoints(rs.getString("pts"));
            top14.setJ(rs.getString("j"));
            top14.setG(rs.getString("g"));
            top14.setN(rs.getString("n"));
            top14.setP(rs.getString("p"));
            top14.setBp(rs.getString("bp"));
            top14.setBc(rs.getString("bc"));
            top14.setDifPoint(rs.getString("diffPoint"));

            listTop14.add(top14);
        }

        return listTop14;
    }

    public List<Classement> getClassementProD2() throws SQLException{
        List<Classement> top14 = new ArrayList<Classement>();
        Statement statement = connection.createStatement();

        String query = "SELECT classement.*, club.nom FROM classement,club WHERE classement.id_Club=club.id AND classement.id_Championnat = 2 ORDER BY classement.rang;";
        ResultSet rs = statement.executeQuery(query);

        while(rs.next()) {

            Classement classement = new Classement();
            classement.setRang(rs.getString("rang"));
            classement.setNomClub(rs.getString("nom"));
            classement.setPoints(rs.getString("pts"));
            classement.setJ(rs.getString("j"));
            classement.setG(rs.getString("g"));
            classement.setN(rs.getString("n"));
            classement.setP(rs.getString("p"));
            classement.setBp(rs.getString("bp"));
            classement.setBc(rs.getString("bc"));
            classement.setDifPoint(rs.getString("diffPoint"));
            top14.add(classement);
        }

        return top14;
    }

    public List<Classement> getClassementTop8() throws SQLException{
        List<Classement> top8 = new ArrayList<Classement>();
        Statement statement = connection.createStatement();

        String query = "SELECT classement.*, club.nom FROM classement,club WHERE classement.id_Club=club.id AND classement.id_Championnat = 3 ORDER BY classement.rang;";
        ResultSet rs = statement.executeQuery(query);

        while(rs.next()) {

            Classement classement = new Classement();
            classement.setRang(rs.getString("rang"));
            classement.setNomClub(rs.getString("nom"));
            classement.setPoints(rs.getString("pts"));
            classement.setJ(rs.getString("j"));
            classement.setG(rs.getString("g"));
            classement.setN(rs.getString("n"));
            classement.setP(rs.getString("p"));
            classement.setBp(rs.getString("bp"));
            classement.setBc(rs.getString("bc"));
            classement.setDifPoint(rs.getString("diffPoint"));
            top8.add(classement);
        }

        return top8;
    }
}
