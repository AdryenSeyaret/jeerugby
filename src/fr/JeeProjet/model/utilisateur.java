package fr.JeeProjet.model;

import fr.JeeProjet.model.UtilisateurModel;

import java.io.PrintWriter;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class utilisateur {
    public utilisateur(){}
    private int id;
    private String pseudo;
    private String email;
    private String mdp;
    private String Confmdp;
    private int civilite;
    private String nom;
    private String prenom;
    private String adresse;
    private String adresseSuite;
    private int cp;
    private String ville;
    private int telephone;
    private int nbabFF;
    private String ddn;
    private String pays;
    private String etat;
    private String catPro;
    private String spoPrat;
    private String equipeFav1;
    private String equipeFav2;
    private String equipeFav3;
    private int newsletter;
    private int offresFf;
    private int isEditeur;
    private int isAdmin;

    public utilisateur(int id, String pseudo, String email, String mdp, String confmdp, int civilite,
                       String nom, String prenom, String adresse, String adresseSuite, int cp, String ville,
                       int telephone, int nbabFF, String ddn, String pays, String etat, String catPro,
                       String spoPrat, String equipeFav1, String equipeFav2, String equipeFav3, int newsletter,
                       int offresFf, int isEditeur, int isAdmin) {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
        this.mdp = mdp;
        Confmdp = confmdp;
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.adresseSuite = adresseSuite;
        this.cp = cp;
        this.ville = ville;
        this.telephone = telephone;
        this.nbabFF = nbabFF;
        this.ddn = ddn;
        this.pays = pays;
        this.etat = etat;
        this.catPro = catPro;
        this.spoPrat = spoPrat;
        this.equipeFav1 = equipeFav1;
        this.equipeFav2 = equipeFav2;
        this.equipeFav3 = equipeFav3;
        this.newsletter = newsletter;
        this.offresFf = offresFf;
        this.isEditeur = isEditeur;
        this.isAdmin = isAdmin;
    }

    private static Connection connection;

    public Connection getConnection() {
        return connection;
    }


    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public int getCivilite() {
        return civilite;
    }

    public void setCivilite(int civilite) {
        this.civilite = civilite;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getAdresseSuite() {
        return adresseSuite;
    }

    public void setAdresseSuite(String adresseSuite) {
        this.adresseSuite = adresseSuite;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public int getNbabFF() {
        return nbabFF;
    }

    public void setNbabFF(int nbabFF) {
        this.nbabFF = nbabFF;
    }

    public String getDdn() {
        return ddn;
    }

    public void setDdn(String ddn) {
        this.ddn = ddn;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getCatPro() {
        return catPro;
    }

    public void setCatPro(String catPro) {
        this.catPro = catPro;
    }

    public String getSpoPrat() {
        return spoPrat;
    }

    public void setSpoPrat(String spoPrat) {
        this.spoPrat = spoPrat;
    }

    public String getEquipeFav1() {
        return equipeFav1;
    }

    public void setEquipeFav1(String equipeFav1) {
        this.equipeFav1 = equipeFav1;
    }

    public String getEquipeFav2() {
        return equipeFav2;
    }

    public void setEquipeFav2(String equipeFav2) {
        this.equipeFav2 = equipeFav2;
    }

    public String getEquipeFav3() {
        return equipeFav3;
    }

    public void setEquipeFav3(String equipeFav3) {
        this.equipeFav3 = equipeFav3;
    }

    public int getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(int newsletter) {
        this.newsletter = newsletter;
    }

    public int getOffresFf() {
        return offresFf;
    }

    public void setOffresFf(int offresFf) {
        this.offresFf = offresFf;
    }

    public int isEditeur() {
        return isEditeur;
    }

    public void setEditeur(int editeur) {
        isEditeur = editeur;
    }

    public int isAdmin() {
        return isAdmin;
    }

    public void setAdmin(int admin) {
        isAdmin = admin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getConfmdp() {
        return Confmdp;
    }

    public void setConfmdp(String confmdp) {
        Confmdp = confmdp;
    }

    public utilisateur(Connection connection) {

        this.connection = connection;
    }

    public Boolean ExisteMail(String lepseudo,String lemdp)throws SQLException{
        UtilisateurModel userModel = new UtilisateurModel();
        Boolean isEnBase =userModel.ExisteEnBase(lepseudo,lemdp,connection);

        return isEnBase;
    }

    public Map GetDatasFromPseudo(String lepseudo, String lemdp)throws SQLException{
        UtilisateurModel userModel = new UtilisateurModel();
        Map hm = new HashMap<>();
        hm =userModel.GetDonnesFromPseudo(lepseudo,lemdp,connection);

        int leId = (int)hm.get("id");
        String lePseudo = (String)hm.get("nomUti");
        String leemail = (String)hm.get("email");
        String UnMdp = (String)hm.get("mdp");
        String UnConfMdp = (String)hm.get("confmdp");
        int Uncivilite = (int)hm.get("civilite");
        String UnNom = (String)hm.get("nom");
        String UnPrenom = (String)hm.get("prenom");
        String UnAdresse = (String)hm.get("adresse");
        String UnAdresseSuite = (String)hm.get("adresseSuite");
        int UncodePostal = (int)hm.get("codePostal");
        String Unville = (String)hm.get("ville");
        int UnTelephone = (int)hm.get("telephone");
        int UnNbaFF= (int)hm.get("nbabFF");
        String Unddn= (String)hm.get("ddn");
        String lePays = (String)hm.get("pays");
        String lEtat = (String)hm.get("etat");
        String laCatPro = (String)hm.get("catPro");
        String leSpoPrat = (String)hm.get("spoPrat");
        String leequiFav1 = (String)hm.get("equiFav1");
        String leequiFav2 = (String)hm.get("equiFav2");
        String leequiFav3 = (String)hm.get("equiFav3");
        int lanewsletter= (int)hm.get("newsletter");
        int lesOffres= (int)hm.get("offresFF");
        int  lesisEditeur= (int)hm.get("isEditeur");
        int lesisAdmin= (int)hm.get("isAdmin");

        this.setId(leId);
        this.setPseudo(lePseudo);
        this.setEmail(leemail);
        this.setMdp(UnMdp);
        this.setConfmdp(UnConfMdp);
        this.setCivilite(Uncivilite);
        this.setNom(UnNom);
        this.setPrenom(UnPrenom);
        this.setAdresse(UnAdresse);
        this.setAdresseSuite(UnAdresseSuite);
        this.setCp(UncodePostal);
        this.setVille(Unville);
        this.setTelephone(UnTelephone);
        this.setNbabFF(UnNbaFF);
        this.setDdn(Unddn);
        this.setPays(lePays);
        this.setEtat(lEtat);
        this.setCatPro(laCatPro);
        this.setSpoPrat(leSpoPrat);
        this.setEquipeFav1(leequiFav1);
        this.setEquipeFav2(leequiFav2);
        this.setEquipeFav3(leequiFav3);
        this.setNewsletter(lanewsletter);
        this.setOffresFf(lesOffres);
        this.setEditeur(lesisEditeur);
        this.setAdmin(lesisAdmin);
        return hm;
    }

    public static utilisateur getUtilisateurById(int id) throws SQLException{
            String query = "select * from utilisateur where id = ?";
            utilisateur user = new utilisateur();
            PreparedStatement psmt = connection.prepareStatement(query);
            psmt.setInt(1, id);
            ResultSet rs = psmt.executeQuery();

            while(rs.next()) {
                user.setId(id);
                user.setPseudo(rs.getString("nom"));
        }
        return user;
    }

    public static boolean createUtilisateur(utilisateur user){
        boolean civilIdent = false;
        if(user.civilite == 1){
            civilIdent = true;
        }
        String tel = String.valueOf(user.telephone);
        try {
            CreateUtilisateur(user.pseudo, user.email, user.mdp, user.Confmdp, civilIdent, user.nom, user.prenom,
                    user.adresse, user.adresseSuite, user.cp, user.ville, tel, user.nbabFF
            );
            civilIdent = true;
        }catch (Exception e){

        }
        return civilIdent;
    }

    public static boolean CreateUtilisateur(String nomUtil,String email,String mdp,String confmdp,Boolean civilite,String nom,String prenom,String adresse,String adresseSuite,int codePostal,String ville,String telephone,int nbabFF)throws SQLException{
        Boolean monretour = true;
        String query = "INSERT INTO `utilisateur`(`id`, `nomUti`, `email`, `mdp`, `confmdp`, `civilite`, `nom`, `prenom`, `adresse`, `adresseSuite`, `codePostal`, `ville`, `telephone`, `nbabFF`, `ddn`, `pays`, `etat`, `catPro`, `spoPrat`, `equiFav1`, `equiFav2`, `equiFav3`, `newsletter`, `offresFF`, `isEditeur`, `isAdmin`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement psmt = connection.prepareStatement(query);
        return true;
    }

    public static utilisateur CreateUtilisateur(String nomUtil,String email,String mdp,String confmdp,Boolean civilite,String nom,String prenom,String adresse,String adresseSuite,int codePostal,String ville,int telephone,int nbabFF,String ddn,String pays,String etat,String catPro,String spoPrat,String equiFav1,String equiFav2,String equiFav3,Boolean newsletter,Boolean offresFF)throws SQLException{
        String query = "INSERT INTO `utilisateur`(`id`, `nomUti`, `email`, `mdp`, `confmdp`, `civilite`, `nom`, `prenom`, `adresse`, `adresseSuite`, `codePostal`, `ville`, `telephone`, `nbabFF`, `ddn`, `pays`, `etat`, `catPro`, `spoPrat`, `equiFav1`, `equiFav2`, `equiFav3`, `newsletter`, `offresFF`, `isEditeur`, `isAdmin`) VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0,0)";
        utilisateur Monutilisateur = new utilisateur();
        Monutilisateur.setPseudo(nomUtil);
        Monutilisateur.setEmail(email);
        Monutilisateur.setMdp(mdp);
        Monutilisateur.setConfmdp(confmdp);

        int genre = 0;
        if(civilite == true){
            genre = 1 ;
        }
        Monutilisateur.setCivilite(genre);
        Monutilisateur.setNom(nom);
        Monutilisateur.setPrenom(prenom);
        Monutilisateur.setAdresse(adresse);
        Monutilisateur.setAdresseSuite(adresseSuite);
        Monutilisateur.setCp(codePostal);
        Monutilisateur.setVille(ville);
        Monutilisateur.setTelephone(telephone);
        Monutilisateur.setNbabFF(nbabFF);
        Monutilisateur.setDdn(ddn);
        Monutilisateur.setPays(pays);
        Monutilisateur.setEtat(etat);
        Monutilisateur.setCatPro(catPro);
        Monutilisateur.setSpoPrat(spoPrat);
        Monutilisateur.setEquipeFav1(equiFav1);
        Monutilisateur.setEquipeFav2(equiFav2);
        Monutilisateur.setEquipeFav3(equiFav3);
        int hasNewsPaper = 0;
        if(newsletter == true){
            hasNewsPaper = 1 ;
        }else {
            hasNewsPaper = 0;
        }
        Monutilisateur.setNewsletter(hasNewsPaper);

        int pub = 0;
        if(offresFF == true){
            pub = 1 ;
        }else {
            pub = 0;
        }
        Monutilisateur.setOffresFf(pub);

        PreparedStatement psmt = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

        psmt.setString(1, nomUtil);
        psmt.setString(2, email);
        psmt.setString(3, mdp);
        psmt.setString(4, confmdp);
        psmt.setBoolean(5, civilite);
        psmt.setString(6, nom);
        psmt.setString(7, prenom);
        psmt.setString(8, adresse);
        psmt.setString(9, adresseSuite);
        psmt.setInt(10, codePostal);
        psmt.setString(11, ville);
        psmt.setInt(12, telephone);
        psmt.setInt(13, nbabFF);
        psmt.setInt(13, nbabFF);
        psmt.setString(14, ddn);
        psmt.setString(15, pays);
        psmt.setString(16, etat);
        psmt.setString(17, catPro);
        psmt.setString(18, spoPrat);
        psmt.setString(19, equiFav1);
        psmt.setString(20, equiFav2);
        psmt.setString(21, equiFav3);
        psmt.setBoolean(22, newsletter);
        psmt.setBoolean(23, offresFF);


        psmt.executeUpdate();

        ResultSet rs = psmt.getGeneratedKeys();

        if (rs.first()) {
            Monutilisateur.setId((int) rs.getLong(1));
        }


        rs.close();
        psmt.close();


        return Monutilisateur;
    }


    @Override
    public String toString() {
        return "utilisateur{" +
                "id=" + id +
                ", pseudo='" + pseudo + '\'' +
                ", email='" + email + '\'' +
                ", mdp='" + mdp + '\'' +
                ", Confmdp='" + Confmdp + '\'' +
                ", civilite=" + civilite +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", adresseSuite='" + adresseSuite + '\'' +
                ", cp=" + cp +
                ", ville='" + ville + '\'' +
                ", telephone=" + telephone +
                ", nbabFF=" + nbabFF +
                ", ddn='" + ddn + '\'' +
                ", pays='" + pays + '\'' +
                ", etat='" + etat + '\'' +
                ", catPro='" + catPro + '\'' +
                ", spoPrat='" + spoPrat + '\'' +
                ", equipeFav1='" + equipeFav1 + '\'' +
                ", equipeFav2='" + equipeFav2 + '\'' +
                ", equipeFav3='" + equipeFav3 + '\'' +
                ", newsletter=" + newsletter +
                ", offresFf=" + offresFf +
                ", isEditeur=" + isEditeur +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
