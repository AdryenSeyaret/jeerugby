package fr.JeeProjet.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;

import java.util.HashMap;
import java.util.Map;

public class UtilisateurModel {

    public Boolean ExisteEnBase(String lepseudo, String lemdp,Connection connection) throws SQLException {
        String query = "select * from utilisateur where nomUti = ? AND mdp = ?";

        PreparedStatement psmt = connection.prepareStatement(query);

        psmt.setString(1, lepseudo);
        psmt.setString(2, lemdp);


        ResultSet rs = psmt.executeQuery();
        String Ligne;
        if (rs.next() ){
            return true;
        }

        return false;
    }

    public Map GetDonnesFromPseudo(String lepseudo, String lemdp,Connection connection) throws SQLException {
        Map hm = new HashMap<>();
        String query = "select * from utilisateur where nomUti = ? AND mdp = ?";

        PreparedStatement psmt = connection.prepareStatement(query);

        psmt.setString(1, lepseudo);
        psmt.setString(2, lemdp);


        ResultSet rs = psmt.executeQuery();
        while(rs.next()) {
            hm.put("id",rs.getInt("id"));
            hm.put("nomUti",rs.getString("nomUti"));
            hm.put("email",rs.getString("email"));
            hm.put("mdp",rs.getString("mdp"));
            hm.put("confmdp",rs.getString("confmdp"));
            hm.put("civilite",rs.getInt("civilite"));
            hm.put("nom",rs.getString("nom"));
            hm.put("prenom",rs.getString("prenom"));
            hm.put("adresse",rs.getString("adresse"));
            hm.put("adresseSuite",rs.getString("adresseSuite"));
            hm.put("codePostal",rs.getInt("codePostal"));
            hm.put("ville",rs.getString("ville"));
            hm.put("telephone",rs.getInt("telephone"));
            hm.put("nbabFF",rs.getInt("nbabFF"));
            hm.put("ddn",rs.getString("ddn"));
            hm.put("pays",rs.getString("pays"));
            hm.put("etat",rs.getString("etat"));
            hm.put("catPro",rs.getString("catPro"));
            hm.put("spoPrat",rs.getString("spoPrat"));
            hm.put("equiFav1",rs.getString("equiFav1"));
            hm.put("equiFav2",rs.getString("equiFav2"));
            hm.put("equiFav3",rs.getString("equiFav3"));
            hm.put("newsletter",rs.getInt("newsletter"));
            hm.put("offresFF",rs.getInt("offresFF"));
            hm.put("isEditeur",rs.getInt("isEditeur"));
            hm.put("isAdmin",rs.getInt("isAdmin"));
        }

        return hm;
    }








}
