package fr.JeeProjet.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Match {
    public int id;
    public int idChamp;
    public Date dateMatch;
    public int idJournee;
    public int idVisiteur;
    public int idDomicile;
    public int scoreDomicile;
    public int scoreVisiteur;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateMatch() {
        return dateMatch;
    }

    public void setDateMatch(Date dateMatch) {
        this.dateMatch = dateMatch;
    }

    public int getIdJournee() {
        return idJournee;
    }

    public void setIdJournee(int idJournee) {
        this.idJournee = idJournee;
    }

    public int getIdVisiteur() {
        return idVisiteur;
    }

    public void setIdVisiteur(int idVisiteur) {
        this.idVisiteur = idVisiteur;
    }

    public int getIdDomicile() {
        return idDomicile;
    }

    public void setIdDomicile(int idDomicile) {
        this.idDomicile = idDomicile;
    }

    public int getScoreDomicile() {
        return scoreDomicile;
    }

    public void setScoreDomicile(int scoreDomicile) {
        this.scoreDomicile = scoreDomicile;
    }

    public int getScoreVisiteur() {
        return scoreVisiteur;
    }

    public void setScoreVisiteur(int scoreVisiteur) {
        this.scoreVisiteur = scoreVisiteur;
    }

    public int getIdChamp() {
        return idChamp;
    }

    public void setIdChamp(int idChamp) {
        this.idChamp = idChamp;
    }

    // Debut des requetes //

    private Connection connection;
    public Match(Connection connection) {

        this.connection = connection;
    }

    public Match(){}

    public List<Match> getAllMatchOfChamp(int idChamp) throws Exception{
        List<Match> allMatchs = new ArrayList<Match>();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM matche WHERE id_championnat = '" + idChamp + "';";
        ResultSet rs = statement.executeQuery(query);

        while(rs.next()) {
            if(rs.getInt("id_championnat") == idChamp) {
                Match match = new Match();
                match.setId(rs.getInt("id"));
                match.setIdJournee(rs.getInt("id_journee"));
                match.setDateMatch(rs.getDate("dateMatche"));
                match.setIdDomicile(rs.getInt("id_club_domicile"));
                match.setIdVisiteur(rs.getInt("id_club_visiteur"));
                match.setScoreDomicile(rs.getInt("scoredom"));
                match.setScoreVisiteur(rs.getInt("scorevisi"));
                match.setIdChamp(rs.getInt("id_championnat"));
                allMatchs.add(match);
            }
        }
        return allMatchs;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", idChamp=" + idChamp +
                ", dateMatch=" + dateMatch +
                ", idJournee=" + idJournee +
                ", idVisiteur=" + idVisiteur +
                ", idDomicile=" + idDomicile +
                ", scoreDomicile=" + scoreDomicile +
                ", scoreVisiteur=" + scoreVisiteur + '}';
    }
}
