package fr.JeeProjet.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
public class Equipe {
    public Equipe(){}
        public String nom;
        public String nomCourt;
        public String pays;
        public String dateFondation;
        public String president;
        public String entraineur;
        public String nomStade;
        public String cap;
        public String stadeAdresse;
        public int idChampionnat;
        public String photoClub;
        public int id;
        public String nomChampionnat;



    public String getNomChampionnat() {
        return nomChampionnat;
    }

    public void setNomChampionnat(String nomChampionnat) {
        this.nomChampionnat = nomChampionnat;
    }

    public String getPhotoClub() {
        return photoClub;
    }

    public void setPhotoClub(String photoClub) {
        this.photoClub = photoClub;
    }

    public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public String getNomCourt() {
            return nomCourt;
        }

        public void setNomCourt(String nomCourt) {
            this.nomCourt = nomCourt;
        }

        public String getPays() {
            return pays;
        }

        public void setPays(String pays) {
            this.pays = pays;
        }

        public String getDateFondation() {
            return dateFondation;
        }

        public void setDateFondation(String dateFondation) {
            this.dateFondation = dateFondation;
        }

        public String getPresident() {
            return president;
        }

        public void setPresident(String president) {
            this.president = president;
        }

        public String getEntraineur() {
            return entraineur;
        }

        public void setEntraineur(String entraineur) {
            this.entraineur = entraineur;
        }

        public String getNomStade() {
            return nomStade;
        }

        public void setNomStade(String nomStade) {
            this.nomStade = nomStade;
        }

        public String getCap() {
            return cap;
        }

        public void setCap(String cap) {
            this.cap = cap;
        }

        public String getStadeAdresse() {
            return stadeAdresse;
        }

        public void setStadeAdresse(String stadeAdresse) {
            this.stadeAdresse = stadeAdresse;
        }

        public int getIdChampionnat() {
            return idChampionnat;
        }

        public void setIdChampionnat(int idChampionnat) {
            this.idChampionnat = idChampionnat;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }


    private Connection connection;
    public Equipe(Connection connection) {

        this.connection = connection;
    }

    public List<Equipe> getListeEquipeByNomChampionnat(int idChamp) throws Exception{
        List<Equipe> listeEquipe = new ArrayList<Equipe>();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM club WHERE id_Championnat = " + idChamp + ";";
        ResultSet rs = statement.executeQuery(query);
        while(rs.next()) {
            Equipe club = new Equipe();
            club.setNom(rs.getString("nom"));
            club.setNomCourt(rs.getString("nomcourt"));
            club.setCap(rs.getString("cap"));
            club.setDateFondation(rs.getString("dateFondation"));
            club.setEntraineur(rs.getString("entraineur"));
            club.setNomStade(rs.getString("nomStade"));
            club.setPays(rs.getString("pays"));
            club.setPresident(rs.getString("president"));
            club.setStadeAdresse(rs.getString("stadeAdresse"));
            club.setPhotoClub(rs.getString("photoClub"));
            club.setIdChampionnat(rs.getInt("id_Championnat"));
            club.setId(rs.getInt("id"));
            listeEquipe.add(club);
        }
        return listeEquipe;
    }

    public List<Equipe> getListeEquipeByIdChampionnat(String nomChampionnat) throws Exception{
        Statement statement = connection.createStatement();
        String q = "SELECT * FROM championnat WHERE nomChampionnat= " + nomChampionnat + ";";
        ResultSet rs1 = statement.executeQuery(q);
        int idChamp = rs1.getInt("id");

        return getListeEquipeByNomChampionnat(idChamp);
    }

    public Equipe getTeamFromName(String name) throws Exception{

        String query = "SELECT * FROM club WHERE nom=" + name;
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(query);
        Equipe club = new Equipe();
        while(rs.next()) {

            club.setNom(rs.getString("nom"));
            club.setNomCourt(rs.getString("nomcourt"));
            club.setCap(rs.getString("cap"));
            club.setDateFondation(rs.getString("dateFondation"));
            club.setEntraineur(rs.getString("entraineur"));
            club.setNomStade(rs.getString("nomStade"));
            club.setPays(rs.getString("pays"));
            club.setPresident(rs.getString("president"));
            club.setStadeAdresse(rs.getString("stadeAdresse"));
            club.setPhotoClub(rs.getString("photoClub"));
            club.setIdChampionnat(rs.getInt("id_Championnat"));
            club.setId(rs.getInt("id"));

        }
        return club;
    }


    public Equipe getTeamFromId(int id) throws Exception{
        Statement statement = connection.createStatement();
        String query = "SELECT club.*,championnat.nomChampionnat FROM club,championnat WHERE club.id_Championnat =championnat.id  AND club.id=" + id+ ";";

        ResultSet rs = statement.executeQuery(query);

        Equipe club = new Equipe();
        while(rs.next()) {
            club.setNom(rs.getString("nom"));
            club.setNomCourt(rs.getString("nomcourt"));
            club.setCap(rs.getString("cap"));
            club.setDateFondation(rs.getString("dateFondation"));
            club.setEntraineur(rs.getString("entraineur"));
            club.setNomStade(rs.getString("nomStade"));
            club.setPays(rs.getString("pays"));
            club.setPresident(rs.getString("president"));
            club.setStadeAdresse(rs.getString("stadeAdresse"));
            club.setPhotoClub(rs.getString("photoClub"));
            club.setIdChampionnat(rs.getInt("id_Championnat"));
            club.setId(rs.getInt("id"));
            club.setNomChampionnat(rs.getString("nomChampionnat"));
        }
        return club;
    }
}
