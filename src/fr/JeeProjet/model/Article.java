package fr.JeeProjet.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Article {
    public Article(){}
    public String titre;
    public String contenu;
    public String dateCreation;
    public int idUtilisateur;
    public String pseudoUtilisateur;
    public int id;
    public String photo;
    public String nomChampionnat;

    public String getNomChampionnat() {
        return nomChampionnat;
    }

    public void setNomChampionnat(String nomChampionnat) {
        this.nomChampionnat = nomChampionnat;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPseudoUtilisateur(String pseudoUtilisateur){
        this.pseudoUtilisateur = pseudoUtilisateur ;
    }

    public String getPseudoUtilisateur(){
        return pseudoUtilisateur;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public Article(Connection connection) {
        this.connection = connection;
    }
    private static Connection connection;

    public static List<Article> getListArticle() throws Exception{
        List<Article> returnList = new ArrayList<Article>();
        Statement statement = connection.createStatement();

        String query = "SELECT article.*,championnat.nomChampionnat FROM article,championnat WHERE article.id_championnat = championnat.id order by article.id DESC";
        ResultSet rs = statement.executeQuery(query);


        while(rs.next()) {
            Article article = new Article();
            article.setTitre(rs.getString("titre"));
            article.setContenu(rs.getString("contenu"));
            article.setDateCreation(rs.getString("dateCreation"));
            article.setPhoto(rs.getString("photo"));
            article.setIdUtilisateur(rs.getInt("id_utilisateur"));
            article.setId(rs.getInt("article.id"));
            article.setNomChampionnat(rs.getString("championnat.nomChampionnat"));
            returnList.add(article);

        }
        return returnList;
    }
    public static List<Article> getListArticleTop14() throws Exception{
        List<Article> returnList = new ArrayList<Article>();
        Statement statement = connection.createStatement();

        String query = "SELECT article.*,championnat.nomChampionnat FROM article,championnat WHERE article.id_championnat = championnat.id AND article.id_championnat = 1 order by article.id DESC";
        ResultSet rs = statement.executeQuery(query);


        while(rs.next()) {
            Article article = new Article();
            article.setTitre(rs.getString("titre"));
            article.setContenu(rs.getString("contenu"));
            article.setDateCreation(rs.getString("dateCreation"));
            article.setPhoto(rs.getString("photo"));
            article.setIdUtilisateur(rs.getInt("id_utilisateur"));
            article.setId(rs.getInt("article.id"));
            article.setNomChampionnat(rs.getString("championnat.nomChampionnat"));
            returnList.add(article);

        }
        return returnList;
    }
    public static List<Article> getListArticleProD2() throws Exception{
        List<Article> returnList = new ArrayList<Article>();
        Statement statement = connection.createStatement();

        String query = "SELECT article.*,championnat.nomChampionnat FROM article,championnat WHERE article.id_championnat = championnat.id AND article.id_championnat = 2 order by article.id DESC";
        ResultSet rs = statement.executeQuery(query);


        while(rs.next()) {
            Article article = new Article();
            article.setTitre(rs.getString("titre"));
            article.setContenu(rs.getString("contenu"));
            article.setDateCreation(rs.getString("dateCreation"));
            article.setPhoto(rs.getString("photo"));
            article.setIdUtilisateur(rs.getInt("id_utilisateur"));
            article.setId(rs.getInt("article.id"));
            article.setNomChampionnat(rs.getString("championnat.nomChampionnat"));
            returnList.add(article);

        }
        return returnList;
    }
    public static List<Article> getListArticleTop8() throws Exception{
        List<Article> returnList = new ArrayList<Article>();
        Statement statement = connection.createStatement();

        String query = "SELECT article.*,championnat.nomChampionnat FROM article,championnat WHERE article.id_championnat = championnat.id AND article.id_championnat = 3 order by article.id DESC";
        ResultSet rs = statement.executeQuery(query);


        while(rs.next()) {
            Article article = new Article();
            article.setTitre(rs.getString("titre"));
            article.setContenu(rs.getString("contenu"));
            article.setDateCreation(rs.getString("dateCreation"));
            article.setPhoto(rs.getString("photo"));
            article.setIdUtilisateur(rs.getInt("id_utilisateur"));
            article.setId(rs.getInt("article.id"));
            article.setNomChampionnat(rs.getString("championnat.nomChampionnat"));
            returnList.add(article);

        }
        return returnList;
    }




    public static String getPseudoUtilisateurById(int id)throws Exception{
        Statement stat = connection.createStatement();
        String query = "SELECT nomUtil FROM utilisateur WHERE id=" + id;
        ResultSet rs = stat.executeQuery(query);
        utilisateur user = new utilisateur();
        while(rs.next()) {
            user.setPseudo(rs.getString("nomUtil"));
        }
        return user.getPseudo();
    }
}
