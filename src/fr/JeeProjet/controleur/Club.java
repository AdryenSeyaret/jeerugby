package fr.JeeProjet.controleur;

import fr.JeeProjet.model.Equipe;
import fr.JeeProjet.model.model;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

@WebServlet(name = "Club")
public class Club extends HttpServlet {
    private Equipe monEquipe= null;
    private Equipe courantEquipe= null;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try (Connection connection = model.getConnection()) {
            monEquipe = new Equipe(connection);
            PrintWriter out = response.getWriter();
            String paramValue = request.getParameter("id");
            if(paramValue == null){
                out.println("Une Erreur de variable est survenue");
            }else{
                int monid = Integer.parseInt(paramValue);

                courantEquipe = monEquipe.getTeamFromId(monid);

                if(courantEquipe.getNom() == null){
                    out.println("Ce club n'existe pas");
                }else{
                    request.setAttribute("Club", courantEquipe);
                    this.getServletContext().getRequestDispatcher("/Club.jsp").forward(request,response);
                }
            }




        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            e.printStackTrace();
            out.println(e);

        }


    }
}
