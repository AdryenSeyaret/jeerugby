package fr.JeeProjet.controleur;

import fr.JeeProjet.model.Article;
import fr.JeeProjet.model.Classement;
import fr.JeeProjet.model.model;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;

@WebServlet(name = "Blog")
public class Blog extends HttpServlet {

    private Article monArticle = null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (Connection connection = model.getConnection()) {


            monArticle= new Article(connection);
            List<Article> listArticle = monArticle.getListArticle();

            HttpSession maSession = request.getSession();
            PrintWriter out = response.getWriter();
            request.setAttribute("listArticle", listArticle);
            this.getServletContext().getRequestDispatcher("/blog.jsp").forward(request,response);
        }catch(Exception e){
            PrintWriter out = response.getWriter();
            e.printStackTrace();
            out.println(e);
        }

    }
}
