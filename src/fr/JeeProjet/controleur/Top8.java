package fr.JeeProjet.controleur;

import fr.JeeProjet.model.Article;
import fr.JeeProjet.model.Classement;
import fr.JeeProjet.model.Equipe;
import fr.JeeProjet.model.model;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;

@WebServlet(name = "Top8")
public class Top8 extends HttpServlet {
    private Equipe MesEquipes= null;
    private Classement monClassement= null;
    private Article monArticle= null;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (Connection connection = model.getConnection()) {
            MesEquipes = new Equipe(connection);
            monClassement = new Classement(connection);
            monArticle= new Article(connection);
            List<Article> listArticle = monArticle.getListArticleTop8();
            List<Classement> ClassementTop8 = monClassement.getClassementTop8();
            List<Equipe> ListeEquipe = MesEquipes.getListeEquipeByNomChampionnat(3);
            HttpSession maSession = request.getSession();

            PrintWriter out = response.getWriter();


            request.setAttribute("ListeEquipe", ListeEquipe);
            request.setAttribute("ClassementTop8", ClassementTop8);
            request.setAttribute("ListeArticle", listArticle);
            this.getServletContext().getRequestDispatcher("/Top8.jsp").forward(request,response);

        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            e.printStackTrace();
            out.println(e);

        }










    }
}
