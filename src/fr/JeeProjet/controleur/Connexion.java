package fr.JeeProjet.controleur;

import fr.JeeProjet.model.utilisateur;
import fr.JeeProjet.model.model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Connexion")
public class Connexion extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (Connection connection = model.getConnection()) {
            String pseudo =request.getParameter( "pseudo" );
            String mdp =request.getParameter( "mdp" );
            PrintWriter out = response.getWriter();

            utilisateur unUtilisateur = new utilisateur(connection);
            Boolean isenBase = unUtilisateur.ExisteMail(pseudo,mdp);
            if(isenBase){
                Map hm = new HashMap<>();
                hm = unUtilisateur.GetDatasFromPseudo(pseudo,mdp);

                HttpSession maSession = request.getSession();
                maSession.setAttribute("utilisateur",unUtilisateur);
                this.getServletContext().getRequestDispatcher("/MonCompte.jsp").forward(request,response);
            }else{
                int myerror = 1;
                request.setAttribute("error",myerror);
                if(request.getParameter("inscrit") == null){
                    request.setAttribute("inscrit",0);
                }
                RequestDispatcher dispatcher;
                this.getServletContext().getRequestDispatcher("/Connexion.jsp").forward(request,response);
            }



        }catch (SQLException | ClassNotFoundException e) {
            PrintWriter out = response.getWriter();
            e.printStackTrace();
            out.println(e);

        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int myerror = 0;
        int inscrit = 0;




        request.setAttribute("error",myerror);
        request.setAttribute("inscrit",inscrit);

        this.getServletContext().getRequestDispatcher("/Connexion.jsp").forward(request,response);
    }
}
