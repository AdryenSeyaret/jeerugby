package fr.JeeProjet.controleur;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;

import fr.JeeProjet.model.Article;
import fr.JeeProjet.model.Classement;
import fr.JeeProjet.model.model;


@WebServlet(name = "AccueilServlet")
public class Accueil extends HttpServlet {
    private Classement monClassement= null;
    private Article monArticle= null;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try (Connection connection = model.getConnection()) {
            monClassement = new Classement(connection);
            monArticle= new Article(connection);

            List<Classement> top14 = monClassement.GetClassementTop14();
            List classementProD2 = monClassement.getClassementProD2();
            List<Article> listArticle = monArticle.getListArticle();


            HttpSession maSession = request.getSession();
            PrintWriter out = response.getWriter();
            out.println(listArticle.size());
            request.setAttribute("listArticle", listArticle);
            request.setAttribute("ClassementTop14", top14);
            request.setAttribute("ClassementProD2", classementProD2);
            out.println(listArticle);
            this.getServletContext().getRequestDispatcher("/index.jsp").forward(request,response);

        } catch (Exception e) {
            PrintWriter out = response.getWriter();
            e.printStackTrace();
            out.println(e);
        }
    }
}
