package fr.JeeProjet.controleur;

import fr.JeeProjet.model.model;
import fr.JeeProjet.model.utilisateur;
import javafx.css.ParsedValue;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Inscription")
public class Inscription extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> erreurs = new HashMap<String, String>();
        String resultat;
        PrintWriter out = response.getWriter();
        String pseudo = "";
        pseudo =request.getParameter( "Pseudo" );
        String email = request.getParameter( "Email" );
        String mdp = request.getParameter( "mdp" );
        String mdpConfirm = request.getParameter( "mdpComfirm" );
        String genre = request.getParameter( "genre" );
        String nom = request.getParameter( "Nom" );
        String prenom = request.getParameter( "Prenom" );
        String adresse = request.getParameter( "Adresse" );
        String adresseSuite = request.getParameter( "AdresseSuite" );
        String ville = request.getParameter( "Ville" );
        int codePostal;
        if(request.getParameter( "Cp" ).equalsIgnoreCase("")){
            codePostal = 0;
        }else{
         codePostal = Integer.parseInt(request.getParameter( "Cp" ));
        }

        String pays = request.getParameter( "pays" );
        String tel = request.getParameter( "Tel" );
        int telephone;
        if(tel.equalsIgnoreCase("")){
            telephone = 0;
        }else{
            telephone = Integer.parseInt(tel);
        }

        String naissance = request.getParameter( "Naissance" );

        int numMagazine;
        if(request.getParameter( "NumMagazine" ) == null){
            numMagazine = 0;
        }else{
            numMagazine= Integer.parseInt(request.getParameter( "NumMagazine" ));
        }

        String equipe1 = request.getParameter( "Equipe1" );
        String equipe2 = request.getParameter( "Equipe2" );
        String equipe3 = request.getParameter( "Equipe3" );
        String sport = request.getParameter( "sport" );
        String niveau = request.getParameter( "niveau" );
        Boolean newsletter = Boolean.valueOf(request.getParameter( "newsletter" ));
        Boolean pub = Boolean.valueOf(request.getParameter( "pub" ));
        try {
            validationEmail( email );
        } catch ( Exception e ) {
            erreurs.put( "Email", e.getMessage() );
        }

        if(pseudo.equalsIgnoreCase("")){
            erreurs.put( "pseudo", "Votre pseudo ne peut pas être vide !" );
        }

        /* Validation des champs mot de passe et confirmation. */
        try {
            validationMotsDePasse( mdp, mdpConfirm );
        } catch ( Exception e ) {
            erreurs.put( "mdp", e.getMessage() );
        }
        if ( erreurs.isEmpty() ) {
            boolean genr;
            if(genre.equalsIgnoreCase("1")){
                genr = true;
            }else{
                genr = false;
            }


            resultat = "Succès de l'inscription.";
            System.out.println("if");
            try (Connection connection = model.getConnection()) {
                utilisateur newUser = new utilisateur(connection);
                utilisateur MonutilsateurCree = new utilisateur(connection);
                MonutilsateurCree =newUser.CreateUtilisateur(pseudo, email, mdp, mdpConfirm, genr, nom, prenom, adresse, adresseSuite, codePostal, ville, telephone, numMagazine, naissance, pays, null, niveau, sport, equipe1, equipe2, equipe3, newsletter, pub);

                int myerror = 1;
                request.setAttribute("error",myerror);
                request.setAttribute( "inscrit", 1);
                this.getServletContext().getRequestDispatcher( "/Connexion.jsp" ).forward( request, response );

            } catch (Exception e) {
                e.printStackTrace();
                out.println(e);
            }

        } else {
            resultat = "Échec de l'inscription.";
            request.setAttribute( "ATT_ERREURS", erreurs );
            request.setAttribute( "ATT_RESULTAT", resultat );
            System.out.println("else");
            //this.getServletContext().getRequestDispatcher( "/Inscription.jsp" ).forward( request, response );
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/Inscription.jsp").forward(request,response);
    }
    private void validationEmail( String email ) throws Exception {
        if ( email != null && email.trim().length() != 0 ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Merci de saisir une adresse mail valide." );
            }
        } else {
            throw new Exception( "Merci de saisir une adresse mail." );
        }
    }
    private void validationMotsDePasse( String motDePasse, String confirmation ) throws Exception{
        if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
            if (!motDePasse.equals(confirmation)) {
                throw new Exception("Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
            } else if (motDePasse.trim().length() < 3) {
                throw new Exception("Les mots de passe doivent contenir au moins 3 caractères.");
            }
        } else {
            throw new Exception("Merci de saisir et confirmer votre mot de passe.");
        }
    }

    private void addNewUserInDB(utilisateur user){
        String sql = "INSERT INTO `utilisateur`(`id`, `nomUti`, `email`, `mdp`, `confmdp`," +
                " `civilite`, `nom`, `prenom`, `adresse`, `adresseSuite`, `codePostal`, `ville`, " +
                "`telephone`, `nbabFF`, `ddn`, `pays`, `etat`, `catPro`, `spoPrat`, `equiFav1`, " +
                "`equiFav2`, `equiFav3`, `newsletter`, `offresFF`, `isEditeur`, `isAdmin`) " +
                "VALUES ("+user.getId()+","+user.getPseudo()+","+user.getEmail()+","+user.getMdp()
                +","+user.getConfmdp()+","+user.getCivilite()+","+user.getNom()+","+user.getPrenom()
                +","+user.getAdresse()+","+user.getAdresseSuite()+","+user.getCp()+","+user.getVille()
                +","+user.getTelephone()+","+user.getNbabFF()+","+user.getDdn()+","+user.getPays()
                +","+user.getEtat()+","+user.getCatPro()+","+user.getSpoPrat()+","+user.getEquipeFav1()
                +","+user.getEquipeFav2()+","+user.getEquipeFav3()+","+user.getNewsletter()+","
                +user.getOffresFf()+","+user.isEditeur()+","+user.isAdmin()+");";
    }
}
